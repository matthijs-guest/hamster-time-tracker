hamster-time-tracker (3.0.2-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster

  [ Raphaël Hertzog ]
  * Fix hamster to work with Python 3.11. Remove call to
    gettext.bind_textdomaine_codeset() which was removed in Python 3.11.
  * Update standards version to 4.6.2, no changes needed.
  * Fix pattern in watch file

 -- Raphaël Hertzog <hertzog@debian.org>  Tue, 10 Jan 2023 10:09:42 +0100

hamster-time-tracker (3.0.2-3) unstable; urgency=medium

  * No change source upload to get into testing.

 -- Raphaël Hertzog <hertzog@debian.org>  Tue, 25 Aug 2020 11:22:27 +0200

hamster-time-tracker (3.0.2-2) unstable; urgency=medium

  * Document CC-BY-SA-3.0 license for help files and SVG icon

 -- Raphaël Hertzog <hertzog@debian.org>  Thu, 13 Aug 2020 00:23:31 +0200

hamster-time-tracker (3.0.2-1) unstable; urgency=medium

  [ Matthijs Kooijman ]
  * Initial release. Closes: #697781

  [ Raphaël Hertzog ]
  * Synchronize the packaging with my own parallel work
  * Modify the HTML report template to use local version of JQuery
  * Install the upstream changelog
  * Add Vcs-* headers and use tracker team as maintainer
  * Add a watch file
  * Fix capitalization of GNOME
  * Use versioned copyright format URI.
  * Update copyright file header to use current field names (Upstream-
    Maintainer => Upstream-Contact)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Improve the copyright file to be more accurate

 -- Raphaël Hertzog <hertzog@debian.org>  Fri, 07 Aug 2020 23:19:32 +0200
